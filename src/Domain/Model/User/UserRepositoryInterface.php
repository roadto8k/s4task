<?php

namespace App\Domain\Model\User;


/**
 * Interface UserRepositoryInterface
 * @package App\Domain\Model\User
 */
interface UserRepositoryInterface
{

    /**
     * @param int $userId
     * @return User
     */
    public function findById(int $userId): ?User;
    
    /**
     * @return int
     */
    public  function findAllCount(): int;

    /**
     * @param int $limitFirstElement
     * @param int $limitCount
     * @return array
     */
    public function findAllByPageLimit(int $limitFirstElement, int $limitCount): array;

    /**
     * @param User $user
     */
    public function save(User $user): void;




}