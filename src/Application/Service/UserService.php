<?php
/**
 * Created by PhpStorm.
 * User: 111
 * Date: 03.12.2018
 * Time: 20:47
 */

namespace App\Application\Service;

use App\Domain\Model\User\UserRepositoryInterface;
use App\Domain\Model\User\User;
use Doctrine\ORM\EntityNotFoundException;


/**
 * Class UserService
 * @package App\Application\Service
 */
final class UserService
{

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;


    /**
     * UserService constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository){
        $this->userRepository = $userRepository;
    }

    /**
     * @param int $userId
     * @return User
     * @throws EntityNotFoundException
     */
    public function getUser(int $userId): User
    {
        $user = $this->userRepository->findById($userId);
        if (!$user) {
            throw new EntityNotFoundException('Пользователя с id'.$userId.'не существует!');
        }

    }

    /**
     * @return int|null
     */
    public function getAllCount(): ?int
    {
        return $this->userRepository->findAllCount();

    }

    /**
     * @param int $limitFirstElement
     * @param int $limitCount
     * @return array|null
     */
    public function getAllByPageLimit(int $limitFirstElement, int $limitCount): ?array
    {
        return $this->userRepository->findAllByPageLimit($limitFirstElement, $limitCount);

    }


    /**
     * @param int $userId
     * @param string $firstName
     * @param string $secondName
     * @param string $middleName
     * @return User
     * @throws EntityNotFoundException
     */
    public function updateUser(int $userId, string $firstName, string $secondName, string $middleName): User
    {
        $user = $this->userRepository->findById($userId);
        if (!$user) {
            throw new EntityNotFoundException('Пользователя с ID '.$userId.' не существует!');
        }

        $user->setFirstName($firstName);
        $user->setSecondName($secondName);
        $user->setMiddleName($middleName);
        $this->userRepository->save($user);

        return $user;
    }

}