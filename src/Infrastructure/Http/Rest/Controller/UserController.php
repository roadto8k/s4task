<?php
/**
 * Created by PhpStorm.
 * User: 111
 * Date: 03.12.2018
 * Time: 22:30
 */

namespace App\Infrastructure\Http\Rest\Controller;


use App\Application\Service\UserService;
use App\Domain\Model\User\User;
use App\Domain\Model\User\UserRepositoryInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use PhpParser\Builder\Property;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Routing\AnnotatedRouteControllerLoader;
use Symfony\Component\Routing\Annotation\Route;




/**
 * Class UserController
 * @package App\Infrastructure\Http\Rest\Controller
 */
class UserController extends FOSRestController
{

    /**
     * @var UserService
     */
    private $userService;

    /**
     * UserController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @Route("/", methods="OPTIONS")
     */
    public function options(Request $request): Response
    {
        $response = new Response();

        $response->headers->set('Access-Control-Allow-Methods', 'OPTIONS, GET');

        return $response;
    }


    /**
     * Retrieves a collection of User resource
     * @Rest\Get("/users/{page}/{count}", name="apiget")
     * @param string $page
     * @param string $count
     * @param Request $request
     * @return View
     */
    public function getUsers($page, $count): View
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
        header('Access-Control-Allow-Headers: content-type');

        $page = intval($page);
        $limitCount = intval($count);

        $totalCount = $this->userService->getAllCount();//общее количество элементов, отправится на фронт для расчета количества страниц в пагинации
        $records['count'] = $totalCount;


        if($page == 1)
        {
            $records['records'] = $this->userService->getAllByPageLimit(0, $limitCount);
        }
        else
        {
            $records['records'] = $this->userService->getAllByPageLimit(($page - 1) *  $limitCount, $limitCount);
        }

        return View::create($records, Response::HTTP_OK);
        //return JsonResponse::create($records, 200, array('Content-Type'=>'application/json; charset=utf-8' ));
    }


    /**
     * @Rest\Put("/user")
     * @param Request $request
     * @return View
     */
    public function putUser(Request $request): View
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: X-Requested-With, content-type');

        print_r($request);

        $user = $this->userService->updateUser($request->get('id'), $request->get('firstName'), $request->get('secondName'),  $request->get('middleName'));

        return View::create($user, Response::HTTP_CREATED);
    }

}