<?php
/**
 * Created by PhpStorm.
 * User: 111
 * Date: 03.12.2018
 * Time: 22:23
 */

namespace App\Infrastructure\Repository;



use App\Domain\Model\User\UserRepositoryInterface;
use App\Domain\Model\User\User;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ArticleRepository
 * @package App\Infrastructure\Repository
 */
final Class UserRepository implements UserRepositoryInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ObjectRepository
     */
    private $objectRepository;

    /**
     * ArticleRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->objectRepository = $this->entityManager->getRepository(User::class);
    }

    /**
     * @param int $userId
     * @return User
     */
    public function findById(int $userId): ?User
    {
        return $this->objectRepository->find($userId);
    }

    /**
     * @return int
     */
    public function findAllCount(): int
    {
        $count = $this->entityManager->createQueryBuilder()
            ->select('count(u.id)')
            ->from('App\Domain\Model\User\User', 'u')
            ->getQuery()
            ->getSingleScalarResult();

        return intval($count);
    }

    /**
     * @param int $limitFirstElement
     * @param int $limitCount
     * @return array
     */
    public function findAllByPageLimit(int $limitFirstElement, int $limitCount): array
    {

        $query = $this->entityManager->createQueryBuilder()
            ->select('u')
            ->from('App\Domain\Model\User\User', 'u')
            ->setFirstResult($limitFirstElement)
            ->setMaxResults($limitCount)
            ->getQuery();

        // returns an array of Product objects
        return $query->execute();
    }


    /**
     * @param User $article
     */
    public function save(User $user): void
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }



}